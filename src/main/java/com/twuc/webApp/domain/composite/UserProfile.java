package com.twuc.webApp.domain.composite;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class UserProfile {

    @Id
    @GeneratedValue
    private Long id;

    @Embedded
    private UserName userName;

    public UserProfile() {
    }

    public UserProfile(UserName userName) {
        this.userName = userName;
    }

    public Long getId() {
        return id;
    }

    public UserName getUserName() {
        return userName;
    }

    public void setUserName(UserName userName) {
        this.userName = userName;
    }
}
